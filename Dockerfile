FROM ubuntu:16.04

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y git

RUN apt-get update && \
    apt-get install -y openjdk-8-jdk && \
    apt-get install -y maven && \
    apt-get install -f libpng16-16 && \
    apt-get install -f libjasper1 && \
    yes | apt-get install -f libdc1394-22

RUN git clone https://github.com/barais/TPDockerSampleApp
WORKDIR TPDockerSampleApp

RUN mvn install:install-file -Dfile=./lib/opencv-3410.jar \
     -DgroupId=org.opencv  -DartifactId=opencv -Dversion=3.4.10 -Dpackaging=jar

EXPOSE 8080

# Pour compiler
RUN mvn clean install && mvn package
# Pour lancer l'application
#RUN java -Djava.library.path=lib/ -jar target/fatjar-0.0.1-SNAPSHOT.jar
#RUN java -Djava.library.path=lib/ubuntuupperthan18/ -jar target/fatjar-0.0.1-SNAPSHOT.jar
ENTRYPOINT [ "java", "-Djava.library.path=lib", "-jar", "/home/ESIRTPDockerSampleApp/target/fatjar-0.0.1-SNAPSHOT.jar"  ]
