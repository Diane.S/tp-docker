# TP Docker
Caroline PINTE ~ IN  
  
Diane SEVIN ~ SI

## Etape 1 - Jouons avec docker, mise en place d'un load balancer et d'un reverse proxy avec docker et nginx 

Dans cette première partie, nous avons testé les différentes fonctionnalités proposées dans le sujet afin découvrir et comprendre les possibilités offertes par Docker.
Nous n'avons pas réussi à faire fonctionner les commandes sur nos machines, nous sommes donc passées par Katacoda pour les points suivants :
* Deploying Your First Docker Container
* Deploy Static HTML Website as Container
* Building Container Images
* Managing Log Files
* Ensuring Uptime
* Docker Metadata & Labels
* Load Balancing Containers
* Orchestration using Docker Compose

## Etape 2 - Utilisation de docker compose 

Ici, nous avons complété le fichier `docker-compose.yml` ci-joint afin de déployer nos 4 services nginx ainsi que le loadbalancer.

## Etape 3 - Dockeriser une application existante 

En partant de l'app web de détection de visages, nous avons créé le fichier `Dockerfile` ci-joint également permettant de créer l'image docker pour cette application.

Pour build ce Dockerfile il faut exécuter la commande suivante :

    docker build -t docker-etape3-build .

Et pour le run, cette commande :

    docker run -d -p 8080:80 docker-etape3-build